<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use DataTables;
use App\Models\Hotel;
use App\Models\user;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class HotelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $data = [
            'menu'       => 'menu.v_menu_admin',
            'content'    => 'content.view_hotel',
            'title'    => 'Table User'
        ];


        if ($request->ajax()) {
            $q_hotel = Hotel::select('*');
            return Datatables::of($q_hotel)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
     
                        $btn = '<div data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="btn btn-sm btn-icon btn-outline-success btn-circle mr-2 edit editUser"><i class=" fi-rr-edit"></i></div>';
                        $btn = $btn.' <div data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-sm btn-icon btn-outline-danger btn-circle mr-2 deleteUser"><i class="fi-rr-trash"></i></div>';
 
                         return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        return view('layouts.v_template', $data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        Hotel::updateOrCreate(['id' => $request->user_id],
        [
         'codigo' => $request->codigo,
         'nome' => $request->nome,
         'tipo' => $request->tipo,
         'imagem' => $request->imagem,
        ]);

        return response()->json(['success'=>'Salvado com Sucesso!']);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $hotel = Hotel::find($id);

        return response()->json($hotel);

    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $res = Hotel::find($id)->delete();
     
        return response()->json($res);
    }
}
