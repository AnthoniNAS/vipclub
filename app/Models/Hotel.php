<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Hotel extends Model
{
    public $timestamps;

    public static function findAllHotel()
    {
        $res = DB::select("SELECT * 
        FROM hotels");     
        return $res;
    }

    public static function createFerias()
    {
        $res = DB::select("SELECT *
        FROM hotels");
        return $res;
    }

    protected $fillable = [
        'codigo',
        'nome',
        'tipo',
        'imagem',
    ];


    public static function edithotel()
    {
        $res = DB::select("SELECT * 
        FROM hotels");     
        return $res;
    }
}
