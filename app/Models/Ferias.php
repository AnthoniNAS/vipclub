<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Ferias extends Model
{
    public $timestamps;

    public static function findAllFerias()
    {
        $res = DB::select("SELECT * 
        FROM ferias");     
        return $res;
    }

    public static function createFerias()
    {
        $res = DB::select("SELECT *
        FROM ferias");
        return $res;
    }

    protected $fillable = [
        'codigo',
        'valor',
        'qtd_hosp',
        'tipo',
    ];


    public static function editFerias()
    {
        $res = DB::select("SELECT * 
        FROM ferias");     
        return $res;
    }
}
