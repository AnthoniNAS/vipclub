<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UsersController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->action([HomeController::class, 'index']);
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/user', [UserController::class, 'index'])->name('user.index');


//Rota Hotel
Route::get('/hotel', 'App\Http\Controllers\HotelController@index')->name('hotel.index');
Route::post('/hotel/cadastro', 'App\Http\Controllers\HotelController@store')->name('hotel.store');
Route::post('/hotel/deletar/{id}', 'App\Http\Controllers\HotelController@destroy')->name('hotel.del');

//Rota Ferias
Route::get('/ferias', 'App\Http\Controllers\FeriasController@index')->name('ferias.index');
Route::post('/ferias/cadastro', 'App\Http\Controllers\FeriasController@store')->name('ferias.store');
Route::post('/ferias/deletar/{id}', 'App\Http\Controllers\FeriasController@destroy')->name('ferias.del');


// Route::get('/user.get_data',[UserController::class, 'get_data'])->name('get_data');
Route::resource('users', UsersController::class);
Route::resource('hotel', 'App\Http\Controllers\HotelController');
Route::resource('ferias', 'App\Http\Controllers\FeriasController');
