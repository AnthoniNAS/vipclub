<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h2>Hoteis</h2>
                <div class="d-flex flex-row-reverse"><button
                        class="btn btn-sm btn-pill btn-outline-primary font-weight-bolder" id="createNewUser"><i
                            class="fas fa-plus"></i>Adicionar </button></div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table" id="tableUser">
                            <thead class="font-weight-bold text-center">
                                <tr>

                                    <th>Codigo</th>
                                    <th>Nome</th>
                                    <th>Tipo</th>
                                    <th>Imagem</th>
                                    <th style="width:90px;">Ação</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                {{-- @foreach ($hoteis as $r_hotel)
                                    <tr>
                                <td>{{$r_hotel->codigo}}</td>
                                <td>{{$r_hotel->nome}}</td>
                                <td>{{$r_hotel->tipo}}</td>
                                <td>{{$r_hotel->imagem}}</td>
                                <td>
                                    <div class="btn btn-success editUser" data-id="{{$r_hotel->id}}">Editar</div>
                                    <div class="btn btn-danger deleteUser" data-id="{{$r_hotel->id}}">Deletar</div>
                                </td>
                                </tr>
                                @endforeach --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Cadastrar-->
<div class="modal fade" id="modal-user" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true" enctype="multipart/form-data">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="exampleModalLabel">Cadastro de Hotel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="formUser" name="formUser" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="text" name="codigo" class="form-control" id="codigo" placeholder="Codigo"><br>
                        <input type="text" name="nome" class="form-control" id="nome" placeholder="Nome"><br>
                        <select name="tipo" class="form-control" id="tipo">
                            <option value="Luxo">Luxo</option>
                            <option value="Premium">Premium</option>
                        </select><br>
                        <input class="form-control form-control-sm" name="imagem" id="imagem" type="file" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary font-weight-bold" id="saveBtn">Salvar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit-->
<div class="modal fade" id="modal-edite" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true" enctype="multipart/form-data">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="exampleModalLabel">Editar Hotel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="formEdit" name="formUser" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="text" name="codigo" class="form-control" id="codigo1" placeholder="codigo"><br>
                        <input type="text" name="nome" class="form-control" id="nome1" placeholder="nome"><br>
                        <select name="tipo" class="form-control" id="tipo1">
                            <option value="Luxo">Luxo</option>
                            <option value="Premium">Premium</option>
                        </select><br>
                        <input class="form-control form-control-sm" name="imagem" id="imagem1" type="file" />
                        <input type="hidden" name="user_id" id="edit_id" value="">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary font-weight-bold" id="saveBtn1">Salvar</button>
            </div>
        </div>
    </div>
</div>


@push('scripts')
<script>
    $('document').ready(function () {
        // success alert
        function swal_success() {
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Salvo com Sucesso!',
                showConfirmButton: false,
                timer: 1000
            })
        }
        // error alert
        function swal_error() {
            Swal.fire({
                position: 'centered',
                icon: 'error',
                title: 'Algo deu errado!',
                showConfirmButton: true,
            })
        }
        // table serverside
        var table = $('#tableUser').DataTable({
            processing: false,
            serverSide: true,
            ordering: false,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'excel', 'pdf'
            ],
            ajax: "{{ route('hotel.index') }}",
            columns: [{
                    data: 'codigo',
                    name: 'codigo'
                },
                {
                    data: 'nome',
                    name: 'nome'
                },
                {
                    data: 'tipo',
                    name: 'tipo'
                },
                {
                    data: 'imagem',
                    name: 'imagem'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ]
        });

        // csrf token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // initialize btn add
        $('#createNewUser').click(function () {
            $('#saveBtn').val("create user");
            $('#user_id').val('');
            $('#formUser').trigger("reset");
            $('#modal-user').modal('show');
        });
        // initialize btn edit
        $('body').on('click', '.editUser', function () {
            var user_id = $(this).data('id');
            $.get("{{route('hotel.index', '')}}" + '/' + user_id + '/edit', function (data) {
                $('#saveBtn1').val("edit-user");
                $('#modal-edite').modal('show');
                $('#edit_id').val(data.id);
                $('#codigo1').val(data.codigo);
                $('#nome1').val(data.nome);
                $('#tipo1').val(data.tipo);
                $('#imagem1').val(data.imagem);
            })
        });
        // initialize btn save
        $('#saveBtn').click(function (e) {
            e.preventDefault();
            $(this).html('Save');

            $.ajax({
                data: $('#formUser').serialize(),
                url: "{{ route('hotel.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data){
                        swal_success();
                        table.draw();
                    }else{
                        swal_error();
                    }
                    $('#formUser').trigger("reset");
                    $('#modal-user').modal('hide');


                },
                error: function (data) {
                    swal_error();
                    $('#saveBtn').html('Save Changes');
                }
            });

        });
        // initialize btn save edit
        $('#saveBtn1').click(function (e) {
            e.preventDefault();
            $(this).html('Save');

            $.ajax({
                data: $('#formEdit').serialize(),
                url: "{{ route('hotel.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data){
                        swal_success();
                        table.draw();
                    }else{
                        swal_error();
                    }
                    $('#formEdit').trigger("reset");
                    $('#modal-edite').modal('hide');


                },
                error: function (data) {
                    swal_error();
                    $('#saveBtn1').html('Save Changes');
                }
            });

        });
        // initialize btn delete
        $('body').on('click', '.deleteUser', function () {
            var hotel_id = $(this).data("id");

            Swal.fire({
                title: 'Tem certeza?',
                text: "Você não poderá reverter isso!",
                icon: 'Aviso!',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sim, exclua!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('hotel.del', '')}}" + '/' + hotel_id,
                        success: function (data) {
                            console.log(data)
                            if(data){
                                swal_success();
                                table.draw();

                            }else{
                                swal_error();
                            }
                        },
                        error: function (data) {
                            swal_error();
                        }
                    });
                }
            })
        });

        // statusing


    });

</script>
@endpush
