<!-- Modal-->
<div class="modal fade" id="modal-user" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true" enctype="multipart/form-data">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="exampleModalLabel">Editar Hotel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                @csrf
                <form id="formUser" name="formUser" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="text" name="codigo" class="form-control" id="codigo" value="{{$hotel->codigo}}"><br>
                        <input type="text" name="nome" class="form-control" id="nome" value="{{$hotel->nome}}"><br>
                        <select name="tipo" class="form-control" id="tipo" value="{{$hotel->tipo}}">
                            <option value="1">Luxo</option>
                            <option value="2">Premium</option>
                        </select><br>
                        <input class="form-control form-control-sm" name="imagem" id="imagem" type="file"></>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary font-weight-bold" id="saveBtn">Salvar</button>
            </div>
        </div>
    </div>
</div>



@push('scripts')
<script>
    $('document').ready(function () {
        // success alert
        function swal_success() {
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Your work has been saved',
                showConfirmButton: false,
                timer: 1000
            })
        }
        // error alert
        function swal_error() {
            Swal.fire({
                position: 'centered',
                icon: 'error',
                title: 'Something goes wrong !',
                showConfirmButton: true,
            })
        }
        // table serverside
        var table = $('#tableUser').DataTable({
            processing: false,
            serverSide: true,
            ordering: false,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'excel', 'pdf'
            ],
            ajax: "{{ route('hotel.index') }}",
            columns: [{
                    data: 'codigo',
                    name: 'codigo'
                },
                {
                    data: 'nome',
                    name: 'nome'
                },
                {
                    data: 'tipo',
                    name: 'tipo'
                },
                {
                    data: 'imagem',
                    name: 'imagem'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ]
        });
        
        // csrf token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // initialize btn save
        $('#saveBtn').click(function (e) {
            e.preventDefault();
            $(this).html('Save');

            $.ajax({
                data: $('#formUser').serialize(),
                url: "{{ route('hotel.atualizar', '') + ['id' => $hotel.id] }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data){
                        swal_success();
                        table.draw();
                    }else{
                        swal_error();
                    }
                    $('#formUser').trigger("reset");
                    $('#modal-user').modal('hide');
                    

                },
                error: function (data) {
                    swal_error();
                    $('#saveBtn').html('Save Changes');
                }
            });

        });
        // statusing


    });

</script>
@endpush
